# Choromage

Choromage is an isometric fantasy role-playing game.The game tells a narrative about how a new wizard Joey learns the ways of magic in this magical world. The game uses world generated scene environments in its open world setting. The narrative allows the player to make their own decisions in morally ambiguous quest lines.

