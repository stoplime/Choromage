﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The animation type that plays. Stores StateOfMind stats (speed, sightistance, etc.) a bool to represent if the animation is cued by a trigger (or just uses a bool) and the name of that trigger/bool 
/// </summary>
// public class StateOfBeing {

// 	private StateOfMind mindset;
// 	private string boolTriggerName;

// 	private bool usesTrigger;

// 	public StateOfMind MindSet
// 	{
// 		 get {return mindset;}
// 	}

// 	public string BoolTriggerName
// 	{
// 		get {return boolTriggerName;}
// 	}

// 	public bool UsesTrigger
// 	{
// 		get {return usesTrigger;}
// 	}

	
// 	public enum Action
// 	{
// 		idle,
// 		moving,
// 		attacking,
// 		hurt,
// 		die
// 	}

// 	// private Action myAction;

// 	// public Action MyAction
// 	// {
// 	// 	get {return myAction;}
// 	// }

// 	public StateOfBeing()
// 	{}

// 	public StateOfBeing(StateOfMind mindState, string triggerName)
// 	{
// 		mindset = mindState;
// 		boolTriggerName = triggerName;
// 	}

// 	public StateOfBeing(StateOfMind mindState, string triggerName, bool cueIsTrigger)
// 	{
// 		mindset = mindState;
// 		boolTriggerName = triggerName;
// 		usesTrigger = cueIsTrigger;
// 	}

// 	// public StateOfBeing(StateOfMind mindState, string triggerName, Action act)
// 	// {
// 	// 	mindset = mindState;
// 	// 	boolTriggerName = triggerName;
// 	// 	action = act;
// 	// }

// 	// public StateOfBeing(StateOfMind mindState, string triggerName, bool cueIsTrigger)
// 	// {
// 	// 	mindset = mindState;
// 	// 	boolTriggerName = triggerName;
// 	// 	usesTrigger = cueIsTrigger;
// 	// }
// }
