﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Element  {

	neutral,	//0
	water,	//1
	earth,	//2
	fire,	//3
	air,	//4
	life,	//5
	death
}

